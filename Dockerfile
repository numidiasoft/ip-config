FROM maven:3.3-jdk-8 AS builder

WORKDIR /app
COPY . /app
RUN mvn clean package


FROM openjdk:8

RUN  adduser java -disabled-password
USER java

WORKDIR /app

COPY --from=builder /app/target/ip-config-0.0.1-SNAPSHOT.jar .


CMD [ "java", "-jar", "/app/ip-config-0.0.1-SNAPSHOT.jar" ]